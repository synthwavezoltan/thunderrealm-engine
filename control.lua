function love.keypressed( key )
  
  
  
  --in player.lua
  if turndef[turn].Can_Move then
    Player_Control(turndef[turn], key) 
    
  end

  if      key == "q" then EndTurn(turn) 
  elseif  key == "u" then Lvl = Lvl+1 Load_Map('DEMO'..Lvl)
  elseif  key == "r" then Save_Metadata('DEMO'..Lvl)
  elseif  key == "z" then if Victory then Lvl = Lvl+1 Load_Map('DEMO'..Lvl) end
  end  
  
end

--regular mouse buttons
function love.mousepressed( x, y, button, istouch )  
  
  --if button == "1" then
      
      --click on playfield
      if  love.mouse.getX() < Field_DrawX
      and love.mouse.getY() < Field_DrawY then
        
        --if there are more steps left than the cost of the current ability
        if turndef[turn].Active_Ability ~= 0
        and (turndef[turn].Energy >= ability_costs[turndef[turn].Active_Ability]) then
          
          --then execute it
          local tiles = Player_CastAbility(turndef[turn],turndef[turn].Active_Ability)
          
          for i=1, #tiles do 
            
            if  tiles[i][1] > 0 and tiles[i][1] <= SizeX
            and tiles[i][2] > 0 and tiles[i][2] <= SizeY 
            
            and Field[tiles[i][1]][tiles[i][2]]%8 > 0
            and Field[tiles[i][1]][tiles[i][2]]%8 < 5     --transformable
            
            and Abst[tiles[i][1]][tiles[i][2]] ~= 6       --shadow tower
            
            and {Player.X, Player.Y} ~= {tiles[i][1], tiles[i][2]}
            and {PlayerTwo.X, PlayerTwo.Y} ~= {tiles[i][1], tiles[i][2]}
            
            then
            
              --Object_Create("Lightning", tiles[i][1], tiles[i][1])
              if Field[tiles[i][1]][tiles[i][2]]%8 == 1
              or Field[tiles[i][1]][tiles[i][2]]%8 == 3
              then Field[tiles[i][1]][tiles[i][2]] = Field[tiles[i][1]][tiles[i][2]] + 1
              else Field[tiles[i][1]][tiles[i][2]] = Field[tiles[i][1]][tiles[i][2]] - 1
              end
              
              
            end
            
          end
          
          --then "pay the costs" and turn off selector
          turndef[turn].Energy = turndef[turn].Energy - ability_costs[turndef[turn].Active_Ability]   
          turndef[turn].Active_Ability = 0

        end
      

      end
      
  --end  

end

--mousewheel
function love.wheelmoved( x, y )
  
  --mouse wheel up
  if y > 0 then

        if turndef[turn].Active_Ability < 10 then
          turndef[turn].Active_Ability = turndef[turn].Active_Ability+1
        end
  end
  
  --mouse wheel down
  if y < 0 then 
    
        if turndef[turn].Active_Ability > 0 then
          turndef[turn].Active_Ability = turndef[turn].Active_Ability-1
        end
  end  


end

--constant actions
function MouseActions()
  
    if love.mouse.isDown(1) then
      
      --click on HUD
      for i=1, 6 do
        --zoom & move away
        local ratio = TileSize/Zoom
        
        if      Navbar[1].Selected and Zoom >= 0  and Zoom > 0.35 then 
          Zoom = Zoom - 0.004
          --Field_OffsetX = Field_OffsetX + ratio 
          --Field_OffsetY = Field_OffsetY + ratio          
        elseif  Navbar[3].Selected                and Zoom < 3 then 
          Zoom = Zoom + 0.004
          --Field_OffsetX = Field_OffsetX - ratio 
          --Field_OffsetY = Field_OffsetY - ratio     
            
        elseif  Navbar[2].Selected then Field_OffsetY = Field_OffsetY + 3 
        elseif  Navbar[4].Selected then Field_OffsetX = Field_OffsetX + 3  
        elseif  Navbar[5].Selected then Field_OffsetY = Field_OffsetY - 3 
        elseif  Navbar[6].Selected then Field_OffsetX = Field_OffsetX - 3         
        end
      
      end
      
    end
  
end

