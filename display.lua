require "player"
require "control"
require "field"

--[[
------------------------------------------------------------------------------------------------
                                    DECLARATOR FUNCTIONS
------------------------------------------------------------------------------------------------
]]--

function Declare_DisplayVars()
  
  --key values
  Zoom = 1
  
  ts_init = 40                    --initializing value for Tileset
	TileSize = ts_init              --size of a displayed tile
  TilesetSize = 8                 --size of a tile on tileset
  
  Field_DrawX = 25*32
  Field_DrawY = 21*32 + 8
  
  Field_OffsetX = 0
  Field_OffsetY = 0
  
  Field_onX = 0
  Field_onY = 0
  
  MousetileX = 0
  MousetileY = 0
  
  --GUI
  Navbar = {}
  GUI_CreateButtons()
  
  --use this code if the playfield is exactly as large as the window
  --SizeX, SizeY = math.floor(winw/TileSize), math.floor(winh/TileSize)
  
	--initializing fonts
  font = {}
  
  for i=1, 10 do font[i] = love.graphics.newFont("slkscre.ttf", 4*i)    end 

	love.graphics.setFont(font[4])	  
  
end

function Create_Tilesets()
 
 	TilesetPics = {}
  
	Tileset = {}         --TilesetPics[1]: playfield tileset
  PlayerSprites = {}   --TilesetPics[2]: player sprite tileset
  GUI_Icons = {}       --TilesetPics[3]: GUI tileset
  Obj_Sprites = {}     --TilesetPics[4]: object tileset
  
  --the third parameter must be equivalent to the size of a tile in the picture, in pixels
	Load_Tilesets(Tileset, 'TILESET.png', TilesetSize)        
  Load_Tilesets(PlayerSprites, 'PLAYERS.png', TilesetSize)  
  Load_Tilesets(GUI_Icons, 'GUI.png', TilesetSize)          
  Load_Tilesets(Obj_Sprites, 'OBJECTS.png', TilesetSize)    
 
end

--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize, id)
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local ImgX = TilesetPics[#TilesetPics]:getWidth()
	local ImgY = TilesetPics[#TilesetPics]:getHeight()
	
	local TilesX = ImgX/UnitSize
	local TilesY = ImgY/UnitSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = 
        love.graphics.newQuad(
          (i-1)*UnitSize, (j-1)*UnitSize, 
          UnitSize, UnitSize, 
          ImgX, ImgY)
		end
	end	
end

--call this in love.update() or love.draw()
function Display_ConstantUpdate()
  
  --TileSize for zoom
  TileSize = ts_init * Zoom
  
  for i=1, 6 do
  
    if  love.mouse.getX() > Navbar[i].X and love.mouse.getX() < Navbar[i].X + Navbar[i].Size 
    and love.mouse.getY() > Navbar[i].Y and love.mouse.getY() < Navbar[i].Y + Navbar[i].Size
    then Navbar[i].Selected = true else Navbar[i].Selected = false end  
    
  end
  
  --which pixel on map do you select
  Field_onX = love.mouse.getX() - Field_OffsetX
  Field_onY = love.mouse.getY() - Field_OffsetY
  
  if  Field_onX > 0 and Field_onX <= SizeX*TileSize
  and Field_onY > 0 and Field_onY <= SizeY*TileSize then
    MousetileX = math.ceil(Field_onX/TileSize)
    MousetileY = math.ceil(Field_onY/TileSize)
  else
    MousetileX = 0
    MousetileY = 0
  
  end
  
end

function GUI_CreateButtons()
  
  for i=1, 3 do 
   Navbar[i]    = {X = Field_DrawX + ((i-1)*76), Y = Field_DrawY-152, Size = 76, Selected = false }
   Navbar[3+i]  = {X = Field_DrawX + ((i-1)*76), Y = Field_DrawY-76, Size = 76, Selected = false }
  end
  
end

--[[
------------------------------------------------------------------------------------------------
                                      DISPLAY FUNCTIONS
------------------------------------------------------------------------------------------------
]]--

--display of any full-sized field, put into a function
function Display_Field(tileset_id, field_name)
  
  TilesetPics[1]:setFilter("nearest", "nearest")
  
  for j=0,SizeX-1 do
    for i=0, SizeY-1 do
      
      local drawtilex = j*TileSize + Field_OffsetX
      local drawtiley = i*TileSize + Field_OffsetY
      
      if  drawtilex > -TileSize and drawtilex < Field_DrawX
      and drawtiley > -TileSize and drawtiley < Field_DrawY then
      love.graphics.draw(TilesetPics[tileset_id], Tileset[field_name[j+1][i+1]], 
                        j*TileSize + Field_OffsetX, 
                        i*TileSize + Field_OffsetY, 
                        0,
                        TileSize/TilesetSize, TileSize/TilesetSize)
      --love.graphics.setFont(font[2])               
      --love.graphics.print(Abst[j+1][i+1], 
      --                  j*TileSize, i*TileSize, 0,
      --                  TileSize/TilesetSize, TileSize/TilesetSize)
      end
    
    end
  end 
  
end

--objects
function Display_Objects()
  
  TilesetPics[4]:setFilter("nearest", "nearest")
  
  for i=1, #O_list do
    love.graphics.draw(TilesetPics[4], O_list[i].Frame, 
                      O_list[i].DrawX, O_list[i].DrawY, 0,
                      TileSize/TilesetSize, TileSize/TilesetSize)
    love.graphics.setFont(font[2])
    love.graphics.print(i, 
                      O_list[i].DrawX, O_list[i].DrawY, 0,
                      TileSize/TilesetSize, TileSize/TilesetSize)                    
  end

end

--player-related gfx
function Display_Players()
  
  Display_Player_Sprite(Player)
  Display_Player_Sprite(PlayerTwo)
  
  --display the ability field only is mouse is on-screen
  if  love.mouse.getX() < Field_DrawX
  and love.mouse.getY() < Field_DrawY 
  then Display_Player_AbilityField() end
  
end

--player sprites
function Display_Player_Sprite(player)
  
  TilesetPics[2]:setFilter("nearest", "nearest")
  
  love.graphics.draw(TilesetPics[2], player.Frame, 
                    player.DrawX, player.DrawY, 0,
                    TileSize/TilesetSize, TileSize/TilesetSize)
                  
              

    
end

--ability field
function Display_Player_AbilityField()
  
  TilesetPics[3]:setFilter("nearest", "nearest")  
  
  if turndef[turn].Active_Ability ~= 0 then
    
    local tiles = Player_CastAbility(turndef[turn],turndef[turn].Active_Ability)
    
    love.graphics.setBlendMode("add")
    
    for i=1, #tiles do
      
      if  tiles[i][1] > 0 and tiles[i][1] <= SizeX
      and tiles[i][2] > 0 and tiles[i][2] <= SizeY then
        
        --display an X if the tile is not transformable
        --as is not within the influence of a shadow tower
        if Field[tiles[i][1]][tiles[i][2]]%8 == 0
        or Field[tiles[i][1]][tiles[i][2]]%8 >= 5
        or Abst[tiles[i][1]][tiles[i][2]] == 6
        
        then
        
          love.graphics.draw(TilesetPics[3], GUI_Icons[5], 
                        (tiles[i][1]-1)*TileSize + Field_OffsetX, 
                        (tiles[i][2]-1)*TileSize + Field_OffsetY, 
                        0, TileSize/TilesetSize, TileSize/TilesetSize)
                     
        else
          
          love.graphics.draw(TilesetPics[3], GUI_Icons[2+turn], 
              (tiles[i][1]-1)*TileSize + Field_OffsetX, 
              (tiles[i][2]-1)*TileSize + Field_OffsetY, 
              0, TileSize/TilesetSize, TileSize/TilesetSize)
                   
        end
      end
    end
    
    love.graphics.setBlendMode("alpha")
    
  end  
  
end


function Display_GUI()
  
  TilesetPics[3]:setFilter("nearest", "nearest") 
  
  local iconsize = 80
  local edgeY = winh-55
  local edgeX = winw-223
  
  --selector
  if MousetileX ~= 0 and MousetileY ~= 0
  and love.mouse.getX() > 0 and love.mouse.getX() < Field_DrawX
  and love.mouse.getY() > 0 and love.mouse.getY() < Field_DrawY then
  love.graphics.draw(TilesetPics[3], GUI_Icons[11], 
                    (MousetileX-1)*TileSize + Field_OffsetX, 
                    (MousetileY-1)*TileSize + Field_OffsetY,0,
                    TileSize/TilesetSize,TileSize/TilesetSize)  
  end
  
  --GUI rectangles
  love.graphics.setColor(76,30,0)
  
  --bottom rect background
  love.graphics.rectangle("fill",0,Field_DrawY, winw, winh-Field_DrawY) 
  
  --right rect background
  love.graphics.rectangle("fill",Field_DrawX, 0, winw-Field_DrawX, Field_DrawY-152)  
  
  --navbar
  for i=1,3 do
    if Navbar[i].Selected 
    then  love.graphics.setColor(255,255,255)
    else  love.graphics.setColor(128,128,128) end
  
    love.graphics.draw(TilesetPics[3], GUI_Icons[7+i], 
                       Navbar[i].X,Navbar[i].Y, 0, 
                        9.5,9.5)  
  
  end
  
  for i=4,6 do
    if Navbar[i].Selected 
    then  love.graphics.setColor(255,255,255)
    else  love.graphics.setColor(128,128,128) end
 
   love.graphics.draw(TilesetPics[3], GUI_Icons[12+i-3], 
                 Navbar[i].X,Navbar[i].Y, 0, 
                  9.5,9.5)

  
  end

  love.graphics.setColor(255,255,255)

  --texts                   
   
  --properties (bottom) 
  love.graphics.print(turndef[turn].Name.."\n"..
                      "Energy left: "..turndef[turn].Energy.."\n"..
                      "Ability: "..turndef[turn].Active_Ability.."\n",
                      40+(5*iconsize),edgeY-28)  
  
  -- the cost
  if turndef[turn].Active_Ability > 0 then
    
    if turndef[turn].Energy < ability_costs[turndef[turn].Active_Ability] then
      love.graphics.setColor(255,0,0)
      love.graphics.print("\n(NOT ENOUGH)",40+(7.5*iconsize),edgeY)
      
    end
    
    love.graphics.print("Step cost: "..ability_costs[turndef[turn].Active_Ability],
                        40+7.5*iconsize,edgeY)
    love.graphics.setColor(255,255,255)
    
  end
   
  --zoom
  love.graphics.setFont(font[6])
  love.graphics.print(math.ceil(Zoom*100,2) .."%", edgeX+64, 4)
  
  --victory-conditional texts
  if Victory then
    love.graphics.setFont(font[10])
    love.graphics.print("Victory!", 20+iconsize, Field_DrawY+8)
    love.graphics.setFont(font[4])
  --love.graphics.print("(aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa)", 10, Field_DrawY+48)
    love.graphics.print("(Press 'Z' for the next map)", 50, Field_DrawY+48)
    
  --defeat-conditional texts
  elseif Defeat then
    love.graphics.setFont(font[10])
    love.graphics.print("GAME OVER!", 20+iconsize, Field_DrawY+8)
    love.graphics.setFont(font[4])
  --love.graphics.print("(aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa)", 10, Field_DrawY+48)
    love.graphics.print("(Restart function is yet\nto be implemented...)", 20+iconsize, Field_DrawY+48) 
    
  end
  
  --energy bars
  love.graphics.setColor(0,0,192)
  love.graphics.rectangle("fill",winw,Field_DrawY, -(Player.Energy * 40), 30)
  love.graphics.setColor(255,255,255)
  
  love.graphics.setFont(font[4])
  
end