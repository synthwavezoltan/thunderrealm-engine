--[[
------------------------------------------------------------------------------------------------
                                    SPECIAL TILE BEHAVIOR
------------------------------------------------------------------------------------------------
]]--

TileName = {}

TileName["Exit"]          = 7        --its behavior is not here since it's player-sensitive
TileName["Shadow Tower"]  = 8
TileName["Shrine"]        = 15

function FieldBehavior(x,y)
  
  --turn-off fields
  Player.Can_Move     = 
  not (Field[Player.X][Player.Y]%8       == 1 or Field[Player.X][Player.Y]%8       == 3)
  
  PlayerTwo.Can_Move  = 
  not (Field[PlayerTwo.X][PlayerTwo.Y]%8 == 1 or Field[PlayerTwo.X][PlayerTwo.Y]%8 == 3)
  
  --shadow tower
  if Field[x][y] == TileName["Shadow Tower"] then
    
    for i=x-3, x+3 do
      for j=y-3, y+3 do
        
        if i > 0 and i <= SizeX and j > 0 and j <= SizeY 
        and (math.abs(i-x)+math.abs(j-y)<5) 
        then
          Abst[i][j] = 6
        end
        
      end
    end   
    
  end
  
end

--[[
------------------------------------------------------------------------------------------------
                                      OBJECT BEHAVIOR
------------------------------------------------------------------------------------------------
]]--

function Object_Declare(o_id, o_type)

  
end

function Object_ConstantUpdate(o_id, o_type)

  O_list[o_id].DrawX = Field_OffsetX + (O_list[o_id].X-1)*TileSize
  O_list[o_id].DrawY = Field_OffsetY + (O_list[o_id].Y-1)*TileSize

end
