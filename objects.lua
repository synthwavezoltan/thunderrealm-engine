Object = {}

Main = {
  X = 0, Y = 0,
  DrawX = 0, DrawY = 0,
  Frame = 0,
  SizeX = 1, SizeY = 1,
}

Map_Metadata = {
  
  }

--this function copies every key-value pairs from an "object" table to a "result_obj"
function Inherit(object)
  local result_obj = {}
  for k, v in pairs(object) do
		result_obj[k] = v
	end 
  return result_obj 
end

--create an object from Object[] table
  function Object_Create(name,x,y) 
    
    local a = Inherit(Object[name]) 
    
    a.X, a.Y = x,y
    
    return a
    
  end

--[[
------------------------------------------------------------------------------------------------
                                      LEVEL-1 OBJECTS
------------------------------------------------------------------------------------------------
]]--



Object.Movable = {
 
 Inherit(Main),
 
 Can_Move = true
 
}

Object.Static = {
  
 Inherit(Main)  
  
}

--[[
------------------------------------------------------------------------------------------------
                                      LEVEL-2 OBJECTS
------------------------------------------------------------------------------------------------
]]--

--children of "Movable"
Object["Player"] = {
  
  Object_Create("Movable"),
  Name = "PlayerName",
  Active_Ability = 0,
  Direction = "up",
  
  Energy = 10,
  Energy_Limit = 10,
  Energy_Rate = 0.1
}

Object["Crate"] = {
  
  Object_Create("Movable"),
  Type = "Crate",
  
}