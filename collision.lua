--set of TILESET tiles that are considered "solid"
SolidTiles = {6, 8, 14, 22}

--needs two numbers: x and y coordinates of the field
function Collision_Basic(x,y, dir)


	local isSolid = false
	local X_Beyond, Y_Beyond = 0,0
  
  --defining block beyond the tile where I want to step
	if dir == "up" then         X_Beyond = x        Y_Beyond = y - 1
	elseif dir == "down" then   X_Beyond = x        Y_Beyond = y + 1	
	elseif dir == "left" then   X_Beyond = x - 1    Y_Beyond = y
	elseif dir == "right" then  X_Beyond = x + 1		Y_Beyond = y
  elseif dir == "self" then   X_Beyond = x	      Y_Beyond = y
	end	
  --gate 0: it's within the field
  if  X_Beyond < 0 and X_Beyond > SizeX
  and Y_Beyond < 0 and Y_Beyond > SizeY
  then isSolid = true else isSolid = false end
  
  
	--gate 1: it's on SolidTiles list
	if isSolid == false then
    for i=1, #SolidTiles do
      if Field[X_Beyond][Y_Beyond] == SolidTiles[i] then	
        isSolid = true
        break
      else
        isSolid = false
      end
    end
  end
	
	--gate 2: there's an object on it	
	if isSolid == false then
		for i=1, #O_list do
			if O_list[i].X == X_Beyond and O_list[i].Y == Y_Beyond then
        if O_list[i].Can_Move then        
          isSolid = true
        else
          isSolid = false
        end
			end
		end
	end

  
	--gate 3: there's a player on it
	if isSolid == false then
		if (turndef[turn] == Player    and (X_Beyond == PlayerTwo.X  and Y_Beyond == PlayerTwo.Y))
    or (turndef[turn] == PlayerTwo and (X_Beyond == Player.X     and Y_Beyond == Player.Y))
    then
			isSolid = true
		else
			isSolid = false
		end	
	end
	
	return isSolid
	
end