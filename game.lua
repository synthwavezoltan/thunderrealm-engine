require "behavior"

function Declare_GameVars()
  
  frametime = 0
  reseter = 0
  
  turn = 1
  
  --game state variables 
  Victory = false
  Defeat = false
  
  EditMode = false
  
end

--declare the turns of player1, player2 and the AI
function Turns()
  
    if  turn == 3 then 
      ComputerTurn()  
      turntext = "AI turn"   
    else
      turntext = turndef[turn].Name
      
    end
 
end

--actions
function ComputerTurn()
  
  if Field[Player.X][Player.Y] == TileName["Shrine"] then
    if    Player.Energy + Player.Energy_Rate <= Player.Energy_Limit
    then  Player.Energy = Player.Energy + Player.Energy_Rate
    else  Player.Energy = Player.Energy_Limit end  
  end
  
  if Field[PlayerTwo.X][PlayerTwo.Y] == TileName["Shrine"] then
    if    PlayerTwo.Energy + PlayerTwo.Energy_Rate <= PlayerTwo.Energy_Limit
    then  PlayerTwo.Energy = PlayerTwo.Energy + PlayerTwo.Energy_Rate
    else  PlayerTwo.Energy = PlayerTwo.Energy_Limit end  
  end  
  
  --end AI turn
  turn = 1

end

function EndTurn()

  if turn == 3 then turn = 1 else turn = turn+1 end

end

function Game_ConstantUpdate()
  
  --declaration of victory
  if    Field[Player.X   ][Player.Y   ] == TileName["Exit"]
  and   Field[PlayerTwo.X][PlayerTwo.Y] == TileName["Exit"]
  
  
  then Victory = true else Victory = false end
  
  --declaration of defeat
  if not (Player.Can_Move or PlayerTwo.Can_Move)
  then Defeat = true else Defeat = false end  
  
  --state of victory
  if Victory then
    Defeat, Player.Can_Move, PlayerTwo.Can_Move = false, false, false
  end
  
  

  
end


--[[
--waiting time at the end of the turn
function RunTurns(framelength)
  
  if timer then
    reseter = reseter + framelength
    
    if reseter >= 0.5 then
      reseter = 0
      timer = false
    end
    
  else
    Turns()        
  end    
  
end  

]]--