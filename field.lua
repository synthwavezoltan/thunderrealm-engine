require "game"

function Declare_FieldVars()
  
  SizeX, SizeY = 20,17
  legend = Load_MapLegend()
  
  --the current lvl
  Lvl = 1  
  
  --field declaration
  Field = Create_Fields(Field)    --tileset values; the very basic "hardcoded" table
  
  Abst = Create_Fields(Abst)      --not entirely displayed, property-carrying table
  Nullify_Field(Abst, 7)
  
  O_list = {}
  
end

--the playing field itself
function Create_Fields(name)
  
	name = { }
	
	for j=1,SizeX do	
		name[j] = { }	
		for i=1, SizeY do
			name[j][i] = 1
		end		
	end
  
    
  
  return name
  
end

function Nullify_Field(tab, value)
 
	for j=1,SizeX do	
		for i=1, SizeY do
			tab[j][i] = value
		end		
	end 
  
end

--loading the meaning of colors from a file
function Load_MapLegend()
  
  local result = {}
  local legend = love.image.newImageData('MAP_LEGEND.png')
  local x,y = legend:getDimensions( )
  local r,g,b,a = 0,0,0,0 
  
  for i=1, x do 
    r,g,b,a = legend:getPixel(i-1,0)
    table.insert(result, {r,g,b,a} )
  end

  return result

end

--associating an RGB color in table "legend" to a tileset value of table "tileset"
function LegendMeaning(r,g,b,a)
    
    for i=1, #legend do
      if    r == legend[i][1] and g == legend[i][2] 
      and   b == legend[i][3] and a == legend[i][4] 
      then  return i end   
    end
    
    
      
end

--loading a map
function Load_Map(name)
  
  local bitmap = love.image.newImageData('/maps/'..name..'.png')
  local x,y = bitmap:getDimensions( )
  
  --globa mapsize variables are now equal to the
  SizeX, SizeY = x,y
  
  for i=1, x do
    
    if Field[i] == nil then Field[i] = {} end
    if Abst[i] == nil then Abst[i] = {} end
    
    for j=1, y do
      Field[i][j] = LegendMeaning(bitmap:getPixel(i-1,j-1))
      Abst[i][j] = 7
    end
  end
  
  Initialize(name) 
  
end

function Field_ConstantUpdate()
  
  	for j=1,SizeX do	
      for i=1, SizeY do
        FieldBehavior(j,i)
      end		
    end 
  
  
  
end