require "useful_functions"
require "display"
require "player"
require "game"
require "control"
require "collision"
require "behavior"
require "field"
require "map_init"
require "objects"

function love.load()

	math.randomseed( os.time() )
  winw, winh = love.window.getMode( )
  
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0	
  
	Declare_DisplayVars()
  Create_Tilesets()
  
  Declare_FieldVars()
	
  Declare_GameVars()
  Declare_Players()
  
  turndef = {Player, PlayerTwo} --1:Player, 2:PlayerTwo
  ability_costs = {1,1,2,2,1,1,2,3,4,5}
  
  turntext = " "
	
  Load_Map('DEMO'..Lvl)
   
end

function love.draw()


  --DAY/NIGHT CICLE
  --if math.floor(frametime/192)%2 == 0
  --then love.graphics.setColor(255,255,255, (frametime%192)+64)
  --else love.graphics.setColor(255,255,255, 192-(frametime%192)+64)
  --end
  
  Display_Field(1, Field)
  Display_Objects()
  Display_Players()
  
  if math.floor(frametime/64)%2 == 0 then
    love.graphics.setColor(255,255,255, ((frametime%64)*4)*0.4)
  else
    love.graphics.setColor(255,255,255, (255-((frametime%64)*4))*0.4)
  end
  
  Display_Field(3, Abst)
  love.graphics.setColor(255,255,255,255)
  
  Display_GUI()

  Display_ConstantUpdate()
  

  
  
  
end

function love.update(dt)
  
  if not Victory then frametime = frametime + 1 end
  --RunTurns(dt)
  
  Player_ConstantUpdate(Player)
  Player_ConstantUpdate(PlayerTwo)
  
  for i=1, #O_list do
    Object_ConstantUpdate(i, O_list[i].Type)
  end
  
  Field_ConstantUpdate()
  
  Game_ConstantUpdate()
  
  Turns()
  
  MouseActions()
  
end


--E:\Documents and Settings\Katamori\Application Data\LOVE\Thunderrealm Game Prototype
function Save_Metadata(name)
	bracket = "{"
	backbracket = "}"
	
	file = love.filesystem.newFile(name .."_metadata.lua")
	asd = file:open("w")
	
  --players  
  for k,v in pairs(Player) do 
    if k == "X" or k == "Y" then file:write( "Player.".. k .." = ".. v .."\n") end
  end
  for k,v in pairs(PlayerTwo) do 
    if k == "X" or k == "Y" then file:write( "PlayerTwo.".. k .." = ".. v .."\n") end
  end  
  
  --objects
  for i=1, #O_list do
    
  end
  
	file:close( )	

end






