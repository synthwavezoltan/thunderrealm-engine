require "objects"

function Declare_Players()

--create two player via inheritance; check object.lua
Player = Object_Create("Player")
Player.Frame = PlayerSprites[1]
Player.Name = "Nathan Minkren"

PlayerTwo = Object_Create("Player")
PlayerTwo.Frame = PlayerSprites[2]
PlayerTwo.Name = "Sanne Lund"

turndef = {Player, PlayerTwo} --1:Player, 2:PlayerTwo

end

function Player_ConstantUpdate(player)
    
  --where to draw
  player.DrawX = Field_OffsetX + (player.X-1)*TileSize
  player.DrawY = Field_OffsetY + (player.Y-1)*TileSize
  
  --count mouse-player-horizon angle
  local angle = math.atan2(player.DrawX-love.mouse.getX(), player.DrawY-love.mouse.getY() )
  
	--change player sprite
	if      180-math.deg(angle) > 315 or  180-math.deg(angle) < 45  then 
    player.Direction = "down" 
	elseif  180-math.deg(angle) < 315 and 180-math.deg(angle) > 225 then 
    player.Direction = "right"
	elseif  180-math.deg(angle) < 225 and 180-math.deg(angle) > 135 then 
    player.Direction = "up"
	elseif  180-math.deg(angle) < 135 and 180-math.deg(angle) > 45  then 
    player.Direction = "left"
	end  
  
end

function Player_Control(character, key)
    
    --every actions that are considered one step
    if key == "w" then
         
        if not Collision_Basic(character.X, character.Y, "up") then
            character.Y = character.Y - 1
            character.Direction = "up"  
        end

    end   
    
    if key == "s" then 
       
       if not Collision_Basic(character.X, character.Y, "down") then 
          character.Y = character.Y + 1 
          character.Direction = "down" 
      end
        
    end  
    
    if key == "a" then 
         
        if not Collision_Basic(character.X, character.Y, "left") then 
          character.X = character.X - 1 
          character.Direction = "left"
      end
      
    end 
    
    if key == "d" then 
             
      if not Collision_Basic(character.X, character.Y, "right") then 
        character.X = character.X + 1 
        character.Direction = "right"  
      end

    end


end



--based on ABILITYSET.PNG
function Player_CastAbility(player,id)
  
  local set = {}
  local entireset = {}
  local tempset = {}
  local abilitymap = love.image.newImageData('ABILITYSET.png')
  
  --loading and processing abilityset.png  
  local ImgX, ImgY = abilitymap:getDimensions( )
  local abmapX, abmapY = 9, 11
  local abstart = (id-1)*abmapX
  
  local which = 0
  
  if player == Player then which = 0 else which = abmapY end
  
  --direction-neutral abilities
  if player.Direction == "left" or player.Direction == "up" then
    
    for i=abstart, abstart+abmapX-1 do
      for j=which, which+abmapY-1 do
        
        local r,g,b,a = abilitymap:getPixel(i,j)
        
        if g == 255 then   
          
          if player.Direction == "left" then
            table.insert(set, {j + player.X - 6 - which, i + player.Y - 4 - abstart} )
          elseif player.Direction == "up" then          
            table.insert(set, {i + player.X - 4 - abstart, j + player.Y - 6 - which} )
          end
        end
        
      end
    end
    
  elseif player.Direction == "right" or player.Direction == "down" then
  
    for i=abstart+abmapX-1, abstart, -1 do
      for j=which+abmapY-1, which, -1 do
        
        local r,g,b,a = abilitymap:getPixel(i,j)
        
        if g == 255 then   
          
          if player.Direction == "right" then
            table.insert(set, {-j + player.X + 6 + which, i + player.Y - 4 - abstart} )
          elseif player.Direction == "down" then          
            table.insert(set, {i + player.X - 4 - abstart, -j + player.Y + 6 + which} )
          end
        end
        
      end
    end  
  
  end

  return set
  
end

